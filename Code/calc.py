import numpy as np
import LHE_format
import time

p_def = LHE_format.particle_def()

def split(p):

    start_time = time.time()

    part_types = [
    'l_',
    'nu_',
    'j_',
    'w_',
    'z_'
    ]
    w_num = z_num = np.array([])
    max_part = int(np.amax(p["event"][:,0]))
    p_out = {}
    p_out_part = {}
    for i in range(max_part):
        for k in range(len(part_types)):
            p_out["{}{}".format(part_types[k], i)] = np.array([])
            p_out_part["{}{}".format(part_types[k], i)] = np.array([])

    for i in range(p["event"].shape[0]):
        number = i
        ll = 0
        lnu = 0
        lj = 0
        lw = 0
        lz = 0
        for k in range(int(p["event"][i, 0])):
            if p["event{}".format(i)][k, 1] == 1:
                if p["event{}".format(i)][k, 0] in p_def["l"]:
                    p_out_part["l_{}".format(ll)] = np.vstack((p_out_part["l_{}".format(ll)],
                    np.append(p["event{}".format(i)][k, 6:10], i))) if len(p_out_part["l_{}".format(ll)]) else np.append(p["event{}".format(i)][k, 6:10], i)
                    ll += 1
                elif p["event{}".format(i)][k, 0] in p_def["nu"]:
                    p_out_part["nu_{}".format(lnu)] = np.vstack((p_out_part["nu_{}".format(lnu)], np.append(p["event{}".format(i)][k, 6:10], i))) if len(p_out_part["nu_{}".format(lnu)]) else np.append(p["event{}".format(i)][k, 6:10], i)
                    lnu += 1
                else:
                    p_out_part["j_{}".format(lj)] = np.vstack((p_out_part["j_{}".format(lj)],
                    np.append(p["event{}".format(i)][k, 6:10], i))) if len(p_out_part["j_{}".format(lj)]) else np.append(p["event{}".format(i)][k, 6:10], i)
                    lj += 1
            if p["event{}".format(i)][k, 1] == 2:
                if p["event{}".format(i)][k, 0] in p_def["w"]:
                    lw += 1
                if p["event{}".format(i)][k, 0] in p_def["z"]:
                    lz += 1
        p_out_part["w_0"] = np.append(p_out_part["w_0"], lw)
        p_out_part["z_0"] = np.append(p_out_part["z_0"], lz)


        if i % 5000 == 0 and i != 0:
            print(i, ", time: ", time.time() - start_time)
            for k in range(max_part):
                p_out["l_{}".format(k)] = np.vstack((p_out["l_{}".format(k)], p_out_part["l_{}".format(k)])) if len(p_out["l_{}".format(k)]) else p_out_part["l_{}".format(k)]

                p_out["nu_{}".format(k)] = np.vstack((p_out["nu_{}".format(k)], p_out_part["nu_{}".format(k)])) if len(p_out["nu_{}".format(k)]) else p_out_part["nu_{}".format(k)]

                p_out["j_{}".format(k)] = np.vstack((p_out["j_{}".format(k)], p_out_part["j_{}".format(k)])) if len(p_out["j_{}".format(k)]) else p_out_part["j_{}".format(k)]

                w_num = np.append(w_num, p_out_part["w_0"])
                z_num = np.append(z_num, p_out_part["z_0"])

                p_out_part["l_{}".format(k)] = np.array([])
                p_out_part["nu_{}".format(k)] = np.array([])
                p_out_part["j_{}".format(k)] = np.array([])
                p_out_part["w_0".format(k)] = np.array([])
                p_out_part["z_0".format(k)] = np.array([])

    for k in range(max_part):
        p_out["l_{}".format(k)] = np.vstack((p_out["l_{}".format(k)], p_out_part["l_{}".format(k)])) if len(p_out["l_{}".format(k)]) else p_out_part["l_{}".format(k)]
        p_out["nu_{}".format(k)] = np.vstack((p_out["nu_{}".format(k)], p_out_part["nu_{}".format(k)])) if len(p_out["nu_{}".format(k)]) else p_out_part["nu_{}".format(k)]
        p_out["j_{}".format(k)] = np.vstack((p_out["j_{}".format(k)], p_out_part["j_{}".format(k)])) if len(p_out["j_{}".format(k)]) else p_out_part["j_{}".format(k)]

        w_num = np.append(w_num, p_out_part["w_0"])
        z_num = np.append(z_num, p_out_part["z_0"])
        p_out_part["w_0".format(k)] = np.array([])
        p_out_part["z_0".format(k)] = np.array([])
    print(i + 1, ", time: ", time.time() - start_time)

    equalizer = np.array([])
    for i in list(p_out):
        if not len(p_out[i]):
            del p_out[i]
        else:
            equalizer = np.append(equalizer, p_out[i].shape[0])

    for i in list(p_out.keys()):
        E = p_out[i][:, 3]
        p_out[i] = np.delete(p_out[i], 3, axis = 1)
        p_out[i] = np.column_stack((E, p_out[i]))

    p_out = fill_gaps(p_out, p["event"].shape[0])
    for i in list(p_out.keys()):
        p_out[i] = np.delete(p_out[i], p_out[i].shape[1] - 1, axis = 1)

    p_out["event"] = p["event"]
    p_out = neutrinoify(p_out)
    p_out = sortify(p_out)
    p_out["w_num"] = w_num
    p_out["z_num"] = z_num

    return(p_out)

def fill_gaps(p, max_size):

    size = np.array([])
    for i in list(p):
        size = np.append(size, p[i].shape[0])
    comp = np.arange(max_size)

    for k in list(p.keys()):
        if p[k].shape[0] != max_size:
            part = np.setdiff1d(comp, p[k][:, 4])
            part2 = np.zeros((len(part), 4))
            part3 = np.column_stack((part2, part))
            p[k] = np.vstack((p[k], part3))
            ind = np.argsort(p[k][:, 4])
            p[k] = p[k][ind]
    return(p)

def neutrinoify(p):
    keys = list(p.keys())
    n_keys = np.array([])
    for i in range(len(keys)):
        if 'n' in list(keys[i]) and 'u' in list(keys[i]):
            n_keys = np.append(n_keys, keys[i])
    nu_arr = np.zeros((p['event'].shape[0], 2))
    for i in range(len(n_keys)):
        nu_arr[:, 0] = np.add(nu_arr[:, 0], p[n_keys[i]][:, 1]) if len(nu_arr[:, 0]) else p[n_keys[i]][:, 1]
        nu_arr[:, 1] = np.add(nu_arr[:, 1], p[n_keys[i]][:, 2]) if len(nu_arr[:, 1]) else p[n_keys[i]][:, 2]
    E = np.sqrt(nu_arr[:, 0] ** 2 + nu_arr[:, 1] ** 2)
    pz = np.zeros((len(E), 1))
    nu_arr = np.column_stack((E, nu_arr))
    nu_arr = np.column_stack((nu_arr, pz))
    p['nu_PT'] = nu_arr

    return(p)

def sortify(p):
    PT = np.array([])
    keys = list(p.keys())
    n_keys = np.array([])
    for i in range(len(keys)):
        if 'l' in list(keys[i]):
            n_keys = np.append(n_keys, keys[i])
    vecs = np.array([])
    for i in range(len(n_keys)):
        part = calc_mult(p[n_keys[i]], 'PT')
        PT = np.column_stack((PT, part)) if len(PT) else part
    sort = np.argsort(-PT, axis = 1)     #descending from left to right
    for i in range(4):
        part = np.array([])
        for k in range(len(n_keys)):
            part = np.column_stack((part, p[n_keys[k]][:, i])) if len(part) else p[n_keys[k]][:, i]
        part = np.take_along_axis(part, sort, axis = 1)
        for k in range(len(n_keys)):
            p[n_keys[k]][:, i] = part[:, k]

    return(p)

def calc_mult(p, type, **kwargs):

    if "ang_part" in kwargs:
        ang_part = kwargs.get("ang_part")
    else:
        ang_part = np.array([1, 1])

    E = px = py = pz = np.zeros(p.shape[0])
    for k in range(int(p.shape[1] / 4)):
        E = np.add(E, p[:, 0 + 4 * k])
        px = np.add(px, p[:, 1 + 4 * k])
        py = np.add(py, p[:, 2 + 4 * k])
        pz = np.add(pz, p[:, 3 + 4 * k])

    if type == 'PT':
        m = (px ** 2 + py ** 2) ** 0.5
    if type == 'Inv_mass':
        m = (E ** 2 - px ** 2 - py ** 2 - pz ** 2) ** 0.5
    if type == 'Pseudorapidity':
        part = (px ** 2 + py ** 2 + pz ** 2) ** 0.5
        m = np.arctanh(np.divide(pz, part))
    if type == 'Angle':
        p1 = np.array([])
        for i in range(ang_part[0]):
            if i == 0:
                p1 = p[:, 1:4]
            if i != 0:
                p1 = np.add(p1, p[:, (i * 4 + 1):(i * 4 + 4)])
        p2 = np.array([])
        for i in range(ang_part[0], ang_part[0] + ang_part[1]):
            if i == ang_part[0]:
                p2 = p[:, (i * 4 + 1):(i * 4 + 4)]
            if i != ang_part[0]:
                p2 = np.add(p2, p[:, (i * 4 + 1):(i * 4 + 4)])

        sca1 = (p1[:, 0] ** 2 + p1[:, 1] ** 2 + p1[:, 2] ** 2) ** 0.5
        sca2 = (p2[:, 0] ** 2 + p2[:, 1] ** 2 + p2[:, 2] ** 2) ** 0.5
        vec1 = np.multiply(p1[:, 0], p2[:, 0])
        vec2 = np.multiply(p1[:, 1], p2[:, 1])
        vec3 = np.multiply(p1[:, 2], p2[:, 2])
        vec = np.add(np.add(vec1, vec2), vec3)


        # sca1 = (p[:, 1] ** 2 + p[:, 2] ** 2 + p[:, 3] ** 2) ** 0.5
        # sca2 = (p[:, 1 + 4] ** 2 + p[:, 2 + 4] ** 2 + p[:, 3 + 4] ** 2) ** 0.5
        # vec1 = np.multiply(p[:, 1], p[:, 1 + 4])
        # vec2 = np.multiply(p[:, 2], p[:, 2 + 4])
        # vec3 = np.multiply(p[:, 3], p[:, 3 + 4])
        # vec = np.add(np.add(vec1, vec2), vec3)
        m = np.arccos(vec / (sca1 * sca2))
        m = np.degrees(m)
    return(m)

def calc_for_draw(data, type, variables):
    p = data
    keys = list(p.keys())
    l_keys = np.array([])
    for i in range(len(keys)):
        if 'l' in list(keys[i]):
            l_keys = np.append(l_keys, keys[i])

def cut(p, cut):
    p_cut = p_stacker(p, cut['particle'])
    distr = calc_mult(p_cut, cut['distribution'])
    width = cut['cut_size'] * cut['cut_size_mult']
    value = cut['cut_around']
    if cut['type'] == 'inside':
        invert = False
    if cut['type'] == 'outside':
        invert = True
    bool_inv = (distr < (value + width)) & (distr > (value - width))  #for z-boson area
    if invert == True:
        bool_inv = np.invert(bool_inv)
    return(bool_inv)

def p_stacker(p, names):
    p_names = np.array([])
    for i in range(len(names)):
        p_names = np.hstack((p_names, p[names[i]])) if len(p_names) else p[names[i]]
    return(p_names)

def z_w_diagram_interpreter(p):
    bools = {}
    bools["w_1"] = p['w_num'] == 1
    bools["w_2"] = p['w_num'] == 2
    bools["z"] = np.invert(np.logical_or(bools["w_1"], bools["w_1"]))
    label = ['1(a)', '2(a-b)', '1(b-c-d)']
    return(bools, label)

def get_max_val(num, values):
    round_amount = 1
    max_arg = np.argmax(num[0])
    max_y = num[0][max_arg]
    max_x = (num[1][max_arg] + num[1][max_arg + 1]) / 2
    dict = {'max_x': max_x}
    dict['median'] = np.median(values)
    dict['mean'] = np.mean(values)
    names = []
    names = np.append(names, 'x(y_max) = {}'.format(round(dict['max_x'], round_amount)))
    names = np.append(names, 'median = {}'.format(round(dict['median'], round_amount)))
    names = np.append(names, 'mean = {}'.format(round(dict['mean'], round_amount)))
    return(dict, names)

def SNR(n):
    list = {}
    noise = 0
    signal = 0
    for i in range(len(n)):
        if i != 0:
            list["values{}".format(i)] = n[i] - n[i-1]
        else:
            list["values{}".format(i)] = n[i]
        list["sum{}".format(i)] = np.sum(list["values{}".format(i)])
        if i != len(n) - 1:
            noise += list["sum{}".format(i)]
        else:
            signal += list["sum{}".format(i)]
    SNR = signal/noise
    print(SNR)
