import os
import shutil
import sys
import numpy as np
import calc
import draw

def draw_data(p, analysis_folder):
    #analysis_folder is the folder 'Analysis'
    #cut_folder is the folder where cuts from cuts.txt are stored
    #

    format = ['png', 'pdf']

    if os.path.isfile(analysis_folder + 'cuts.txt') == False:
        py_folder = os.path.dirname(os.path.abspath(__file__))
        shutil.copy(py_folder + '/cuts.txt', analysis_folder)
        sys.exit("Analysis files were copied. Update their information and run program again.")
    if os.path.isfile(analysis_folder + 'draw.txt') == False:
        py_folder = os.path.dirname(os.path.abspath(__file__))
        shutil.copy(py_folder + '/draw.txt', analysis_folder)
        sys.exit("Analysis files were copied. Update their information and run program again.")
    for_stack, label = calc.z_w_diagram_interpreter(p)
    cuts_file = read(analysis_folder + 'cuts.txt')
    draw_file = read(analysis_folder + 'draw.txt')
    cut = cut_read(cuts_file)
    draw_dict = draw_read(draw_file)
    for i in cut.keys():
        if cut[i]["cut"] == True:
            del_rows = calc.cut(p, cut[i]) #delete rows that have these values False
            cut_folder = cut[i]['folder_name']
        if cut[i]["cut"] == False:
            del_rows = np.array([])
            cut_folder = 'no_cut'
        if os.path.exists(analysis_folder + cut_folder) == False:
            os.mkdir(analysis_folder + cut_folder)

        for k in draw_dict.keys():

            p_values = calc.p_stacker(p, draw_dict[k]['particle'])
            values = calc.calc_mult(p_values, draw_dict[k]['type'], ang_part = draw_dict[k]['ang_part'])
            if cut_folder != 'no_cut':
                values = values[del_rows]
            name_draw = listToStr = '+'.join([str(elem) for elem in draw_dict[k]['particle']])
            print(draw_dict[k]['type'], ', ', name_draw)
            if 'xmin' in draw_dict[k]:
                xmin = draw_dict[k]['xmin']
            else:
                xmin = None
            if 'xmax' in draw_dict[k]:
                xmax = draw_dict[k]['xmax']
            else:
                xmax = None
            stack_fol = analysis_folder + cut_folder + '/' + 'stacked'
            unstack_fol = analysis_folder + cut_folder + '/' + 'unstacked'
            if os.path.exists(stack_fol) == False:
                os.mkdir(stack_fol)
            if os.path.exists(unstack_fol) == False:
                os.mkdir(unstack_fol)
            out_fol = unstack_fol + '/' + draw_dict[k]['type']
            out_fol_stack = stack_fol + '/' + draw_dict[k]['type']

            params = draw.draw(folder_default = out_fol, name = name_draw, values = values, format = format, xlabel = draw_dict[k]['x_label'], dimension = draw_dict[k]['dimension'], xmin = xmin, xmax = xmax)

            params['folder_default'] = params['folder'] = out_fol_stack
            stacked = []
            for l in for_stack.keys():
                part = for_stack[l]
                if cut_folder != 'no_cut':
                    part = part[del_rows]
                x = values[part]
                stacked.append(x)
            draw.draw_stacked(values = stacked, params = params, label = label)


def read(file):
    f = open(file, mode = 'r')
    new = np.array([])
    # read all lines at once
    all_of_it = f.read()
    all_of_it = all_of_it.splitlines()
    for i in all_of_it:
        j = i.replace(" ", "")
        if j != '':
            j = j.split('=')
            new = np.append(new, j)

    # close the file
    f.close()
    return(new)

def cut_read(cuts_file):
    iter = 0
    cuts = {}
    for i in range(len(cuts_file)):
        if cuts_file[i] == "cut":
            cut = {}
            if cuts_file[i + 1] == "True":
                cut["cut"] = True
                while cuts_file[i] != "<sep>":
                    if cuts_file[i] == "type":
                        cut["type"] = cuts_file[i + 1]
                    if cuts_file[i] == "cut_around":
                        cut["cut_around"] = float(cuts_file[i + 1])
                    if cuts_file[i] == "cut_size":
                        cut["cut_size"] = float(cuts_file[i + 1])
                    if cuts_file[i] == "cut_size_mult":
                        cut["cut_size_mult"] = int(cuts_file[i + 1])
                    if cuts_file[i] == "folder_name":
                        cut["folder_name"] = cuts_file[i + 1]
                    if cuts_file[i] == "distribution":
                        cut["distribution"] = cuts_file[i + 1]
                    if cuts_file[i] == "particle":
                        cut["particle"] = cuts_file[i + 1].split("+")
                    i += 1
                cuts["{}".format(iter)] = cut
                iter += 1
            else:
                cut["cut"] = False
                cuts["{}".format(iter)] = cut
                iter += 1
    return(cuts)

def draw_read(cuts_file):
    iter = 0
    cuts = {}
    for i in range(len(cuts_file)):
        if cuts_file[i] == "type":
            type = cuts_file[i + 1]
            while cuts_file[i] != "<sepsep>":
                cut = {}
                while cuts_file[i] != "<sep>":
                    cut["type"] = type
                    if cuts_file[i] == "particle":
                        cut["particle"] = cuts_file[i + 1].split('+')
                    if cuts_file[i] == "xmin":
                        cut["xmin"] = float(cuts_file[i + 1])
                    if cuts_file[i] == "xmax":
                        cut["xmax"] = float(cuts_file[i + 1])
                    if cuts_file[i] == "x_label":
                        cut["x_label"] = cuts_file[i + 1]
                    if cuts_file[i] == "ang_part":
                        cut["ang_part"] = np.array(cuts_file[i + 1].split('+')).astype(np.int)
                    elif "ang_part" not in cut.keys():
                        cut["ang_part"] = np.array([1, 1])
                    i += 1
                i += 1
                cuts["{}".format(iter)] = cut
                iter += 1

    for i in cuts.keys():
        if cuts[i]["type"] == "Inv_mass":
            one = r'$\mathfrak{{M}}_{}$'.format(cuts[i]['x_label'])
            cuts[i]["x_label"] = one
            cuts[i]["dimension"] = "[GeV]"
        if cuts[i]["type"] == "PT":
            one = r'$PT_{}$'.format(cuts[i]['x_label'])
            cuts[i]["x_label"] = one
            cuts[i]["dimension"] = "[GeV]"
        if cuts[i]["type"] == "Pseudorapidity":
            one = r'$\eta_{}$'.format(cuts[i]['x_label'])
            cuts[i]["x_label"] = one
            cuts[i]["dimension"] = ""
        if cuts[i]["type"] == "Angle":
            one = r'$\phi_{}$'.format(cuts[i]['x_label'])
            cuts[i]["x_label"] = one
            cuts[i]["dimension"] = r'$^{\circ}$'
    return(cuts)
