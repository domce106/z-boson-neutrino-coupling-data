def LHEFormat():
    Event = [
    'nparticles', #0 # number of particles
    'subprocess', #1 #number of subprocesses
    'weight',   #2
    'scale',    #3
    'qedcoupling',  #4
    'qcdcoupling'   #5
    ]

    Particle = [
    'pdgid',    #0
    'status',   #1
    'mother_a', #2
    'mother_b', #3
    'color_a',  #4
    'color_b',  #5
    'px',       #6
    'py',       #7
    'pz',       #8
    'E',        #9
    'M',        #10
    'lifetime', #11
    'spin'      #12
    ]

    return(Event, Particle)

def particle_def():
    p_def = {}

    p_def["nu"] = [12, -12, 14, -14, 16, -16]
    p_def["l"] = [11, -11, 13, -13, 15, -15]

    p_def["nu-"] = [12, 14, 16]
    p_def["nu+"] = [-12, -14, -16]

    p_def["l-"] = [11, 13, 15]
    p_def["l+"] = [-11, -13, -15]

    p_def["e-"] = [11]
    p_def["e+"] = [-11]
    p_def["mu-"] = [13]
    p_def["mu+"] = [-13]
    p_def["tau-"] = [15]
    p_def["tau+"] = [-15]

    p_def["nu_e-"] = [12]
    p_def["nu_e+"] = [-12]
    p_def["nu_mu-"] = [14]
    p_def["nu_mu+"] = [-14]
    p_def["nu_tau-"] = [16]
    p_def["nu_tau+"] = [-16]

    p_def["w"] = [24, -24]
    p_def['z'] = [23]
    p_def["gama"] = [22]

    p_def['w+'] = [24]
    p_def['w-'] = [-24]

    return(p_def)
