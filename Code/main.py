import LHE_read
import numpy as np
import os
import calc
import sys
import output

data = "/home/domce106/Work/Events/pp_l+l-vlvl~_2_13TeV/Events/run_06/unweighted_events.lhe.gz"
#data = "/home/domce106/Work/Events/pp_vlvl~_l+l-/Events/run_01/unweighted_events.lhe.gz"

#make analysis folder
fileloc = np.array(data.split('/')[:-1])
newfile = '/'.join(np.append(fileloc, 'Analysis/'))
if os.path.exists(newfile) == False:
    os.mkdir(newfile)

#get unweighted events name
name_of_data = np.array(data.split('/')[-1])
name_of_data = str(name_of_data)[:-7]

file_save = newfile + name_of_data + ".npy"
if os.path.exists(file_save) == False:
    open(newfile + name_of_data, 'a').close()
    p = LHE_read.read(filepath = data)
    p_out = calc.split(p)
    LHE_read.write(p_out, file_save)
else:
    p_out = LHE_read.read_csv(file_save)
output.draw_data(p_out, newfile)

# rules = '/home/domce106/Work/lhereader-2.0/rules.txt'
# LHE_read.output(rules = rules, folder = newfile, data = p_out)
#rules = sys.argv[1]
