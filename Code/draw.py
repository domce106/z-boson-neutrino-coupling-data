import numpy as np
import matplotlib.pyplot as plt
import math
import os
import calc

layout = {'fontsize' : 18, 'x' : .9, 'y' : .85}
alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j']
format = 'png'
dpi = 300
colour = '#0c9deb'
alpha_line = .9
linewidth = .95
line_col = ['black', 'olive', 'fuchsia']

#names of available arguments
arguments = [
'bins',
'folder',
'xmin',
'xmax',
'normed',
'ylabel',
'dimension',
]

def draw(folder_default, values, name, xlabel, format, **kwargs):

    if os.path.exists(folder_default) == False:
        os.mkdir(folder_default)
    #default argument values
    bins = int(len(values)**.5)
    folder = folder_default
    xmin = values.min()
    xmax = values.max()
    normed = False
    ylabel = 'Events'
    dimension = ', [GeV]'
    stacked = False

    #parse through custom arguments and change default values
    for i in range(len(arguments)):
        if kwargs.get(arguments[i]) != None and arguments[i] == 'bins':
            bins = kwargs.get('bins') * bins
        if kwargs.get(arguments[i]) != None and arguments[i] == 'folder':
            folder = kwargs.get('folder')
        if kwargs.get(arguments[i]) != None and arguments[i] == 'xmin':
            xmin = kwargs.get('xmin')
        if kwargs.get(arguments[i]) != None and arguments[i] == 'xmax':
            xmax = kwargs.get('xmax')
        if kwargs.get(arguments[i]) != None and arguments[i] == 'normed':
            normed = kwargs.get('normed')
        if kwargs.get(arguments[i]) != None and arguments[i] == 'ylabel':
            ylabel = kwargs.get('ylabel')
        if kwargs.get(arguments[i]) != None and arguments[i] == 'dimension':
            if kwargs.get('dimension') != '':
                dimension = ', ' + kwargs.get('dimension')
            else:
                dimension = ''
    values_save = np.copy(values)
    values = values[(values >=  xmin) & (values <=xmax)]
    bins = int(2 * (len(values) ** 0.333))

    num = plt.hist(values, bins = bins, facecolor=colour, alpha = .8)
    draw_extra, line_names = calc.get_max_val(num, values_save)
    k = 0
    for i in draw_extra.keys():
        plt.axvline(x = draw_extra[i], label = line_names[k], color = line_col[k], alpha = alpha_line, linewidth = linewidth)
        k += 1
    plt.legend(prop={'size':8})
    plt.xlabel('{}{}'.format(xlabel, dimension), **layout)
    plt.xlim(xmin, xmax)
    plt.ylabel(ylabel, **layout)
    plt.tight_layout(.5)
    for i in range(len(format)):
        plt.savefig('{}/{}.{}'.format(folder, name, format[i]), format = format[i], dpi = dpi)
    plt.close()
    params = {
    'folder_default' : folder_default,
    'name' : name,
    'xlabel' : xlabel,
    'format' : format,
    'bins' : bins,
    'folder' : folder,
    'xmin' : xmin,
    'xmax' : xmax,
    'normed' : normed,
    'ylabel' : ylabel,
    'dimension' : dimension,
    'extra_draws' : draw_extra,
    'line_names' : line_names
    }
    return(params)

def draw_stacked(values, params, label):
    colour_stacked = ['blue', 'red', 'violet']
    folder_default = params['folder_default']
    name = params['name']
    xlabel = params['xlabel']
    format = params['format']
    bins = params['bins']
    folder = params['folder']
    xmin = params['xmin']
    xmax = params['xmax']
    normed = params['normed']
    ylabel = params['ylabel']
    dimension = params['dimension']
    draw_extra = params['extra_draws']
    line_names = params['line_names']
    if os.path.exists(folder_default) == False:
        os.mkdir(folder_default)
    for i in range(len(values)):
        values[i] = values[i][(values[i] >=  xmin) & (values[i] <=xmax)]
    num_hist, bins_hist, patches_hist = plt.hist(values, bins = bins, color = colour_stacked, alpha = .8, stacked = True, label = label)
    calc.SNR(num_hist)
    k = 0
    for i in draw_extra.keys():
        plt.axvline(x = draw_extra[i], label = line_names[k], color = line_col[k], alpha = alpha_line, linewidth = linewidth)
        k += 1
    plt.xlabel('{}{}'.format(xlabel, dimension), **layout)
    plt.xlim(xmin, xmax)
    plt.ylabel(ylabel, **layout)
    plt.legend(prop={'size':8})
    plt.yscale('log')
    plt.tight_layout(.5)
    for i in range(len(format)):
        plt.savefig('{}/{}.{}'.format(folder, name, format[i]), format = format[i], dpi = dpi)
    plt.close()
