import re
import numpy as np
import gzip
import LHE_format
import time
import random
import ast
import os
from matplotlib.offsetbox import AnchoredText as ax
import matplotlib.pyplot as plt


bools = {}
bools["z"] = 3
bools["w_2"] = 2
bools["w_1"] = 1

label = ['2(a-b)', '1(a)', '1(b-c-d)']

for i in bools.keys():
    print(i)
