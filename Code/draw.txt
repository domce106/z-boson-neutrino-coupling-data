type = Inv_mass

particle = l_0 + l_1
xmin = 0
xmax = 300
x_label = {\ell_{1}\ell_{2}}
<sep>

particle = l_0 + nu_PT
xmin = 0
xmax = 300
x_label = {{\ell_{1}\nu_{(1,2)}}}
<sep>

particle = l_1 + nu_PT
xmin = 0
xmax = 300
x_label = {{\ell_{2}\nu_{(1,2)}}}
<sep>

particle = l_0 + l_1 + nu_PT
xmin = 0
xmax = 400
x_label = {{\ell_{1}\ell_{2}\nu_{(1,2)}}}
<sep>
<sepsep>

---------------------------------------

type = PT

particle = l_0
xmin = 0
xmax = 100
x_label = {\ell_1}
<sep>

particle = l_1
xmin = 0
xmax = 100
x_label = {\ell_2}
<sep>

particle = l_0 + l_1
xmin = 0
xmax = 100
x_label = {\ell_{1}\ell_{2}}
<sep>

particle = l_0 + nu_PT
xmin = 0
xmax = 100
x_label = {{\ell_{1}\nu_{(1,2)}}}
<sep>

particle = l_1 + nu_PT
xmin = 0
xmax = 100
x_label = {{\ell_{2}\nu_{(1,2)}}}
<sep>

particle = l_0 + l_1 + nu_PT
xmin = 0
xmax = 100
x_label = {{\ell_{1}\ell_{2}\nu_{(1,2)}}}
<sep>

particle = nu_PT
xmin = 0
xmax = 100
x_label = {\nu_{(1,2)}}
<sep>
<sepsep>

---------------------------------------

type = Pseudorapidity

particle = l_0
x_label = {\ell_1}
<sep>

particle = l_1
x_label = {\ell_2}
<sep>

particle = l_0 + l_1
x_label = {\ell_{1}\ell_{2}}
<sep>

particle = l_0 + nu_PT
x_label = {{\ell_{1}\nu_{(1,2)}}}
<sep>

particle = l_1 + nu_PT
x_label = {{\ell_{2}\nu_{(1,2)}}}
<sep>

particle = nu_PT
x_label = {\nu_{(1,2)}}
<sep>
<sepsep>

---------------------------------------

type = Angle

particle = l_0 + nu_PT
x_label = {{\ell_{1}\nu_{(1,2)}}}
<sep>

particle = l_1 + nu_PT
x_label = {{\ell_{2}\nu_{(1,2)}}}
<sep>

particle = l_0 + l_1
x_label = {\ell_{1}\ell_{2}}
<sep>

<sepsep>

---------------------------------------
