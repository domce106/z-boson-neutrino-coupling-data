import re
import numpy as np
import gzip
import LHE_format
import time
import csv
import pickle
import sys
import ast

def read(filepath, **kwargs):

    if "sample" in kwargs.keys():
        sample = kwargs["sample"]
    else: sample = False
    start_time = time.time()
    sample_size = 30000

    EVENT_TAG = '<event>'
    INIT_TAG = '<init>'
    INIT_END_TAG = '</init>'
    FILE_END_TAG = '</LesHouchesEvents>'
    Event, Particle = LHE_format.LHEFormat()

    fp = gzip.open(filepath, 'rt')
    p = {} #define dict for the whole process
    l1 = 0
    line = fp.readline()

    p["event"] = np.array([])
    partial = np.array([])
    number = 0
    len_particle = len(Particle)

    while line.strip() != FILE_END_TAG :  # Reading the events
        if line.strip() == EVENT_TAG:
            line = fp.readline()
            line = re.sub('\s+', ' ', line).strip().split(' ')
            line[0:2] = map(int, line[0:2])
            line[2:6] = map(float, line[2:6])
            partial = np.vstack((partial, line)) if len(partial) else line

            p["event{}".format(number)] = np.array([])
            for i in range(line[0]):
                line = fp.readline()
                line = re.sub('\s+', ' ', line).strip().split(' ')
                line[0:6] = map(int, line[0:6])
                line[6:13] = map(float, line[6:13])
                p["event{}".format(number)] = np.vstack((p["event{}".format(number)], line)) if len(p["event{}".format(number)]) else line

            if (number % 5000) == 0 and number != 0:
                print(number, ", time: ", time.time() - start_time)
                p["event"] = np.vstack((p["event"], partial)) if len(p["event"]) else partial
                partial = np.array([])
            number += 1
            if sample == True and number == sample_size:
                break

        line = fp.readline()
    print(number, ", time: ", time.time() - start_time)
    p["event"] = np.vstack((p["event"], partial)) if len(p["event"]) else partial

    fp.close()
    return(p)

def write(p, file):
    np.save(file, p)

def read_csv(file):
    p = np.load(file, allow_pickle = 'TRUE').item()
    return(p)
